import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import Profit from '@/views/Profit.vue'
import My from '@/views/My.vue'
import Settlement from '@/views/Settlement.vue'
import Bindingnumber from '@/views/Bindingnumber.vue'
import Shopinfo from '@/views/home/Shopinfo.vue'
import Confirmorder from '@/views/home/Confirmorder.vue'
import Addresschg from '@/views/my/Addresschg.vue'
import Addressadd from '@/views/my/Addressadd.vue'
import Storehouse from '@/views/my/Storehouse.vue'
import Lowerprofit from '@/views/my/Lowerprofit.vue'
import Myaddress from '@/views/my/Myaddress.vue'
import Mylower from '@/views/my/Mylower.vue'
import Storeinfo from '@/views/my/Storeinfo.vue'
import Allorder from '@/views/my/Allorder.vue'
import Orderinfo from '@/views/my/Orderinfo.vue'
import Aftereplenish from '@/views/my/Aftereplenish.vue'
import Tixinfo from '@/views/profit/Tixinfo.vue'
import Shyinfo from '@/views/profit/Shyinfo.vue'
import Sellxiaoshou from '@/views/profit/Sellxiaoshou.vue'
import Usezhichu from '@/views/profit/Usezhichu.vue'
import Yongjinfo from '@/views/profit/Yongjinfo.vue'
import cart from '@/views/cart/cart.vue'

Vue.use(VueRouter)

const routes = [
  // 首页
  {
    path: '/home',
    component: Home,
  },
  //购物车
  {
    path:'/cart',
    component:cart
  },
  // 商品详情
  {
    path: '/shopinfo/:shopid',
    component: Shopinfo
  },
  // 入驻申请
  {
    path: '/settlement',
    component: Settlement
  },
  // 收益
  {
    path: '/profit',
    component: Profit
  },
  // 我的
  {
    path: '/my',
    component: My
  },
  // 确认订单
  {
    path: '/confirmorder',
    component: Confirmorder
  },
  // 我的订单
  {
    path: '/allorder',
    component: Allorder
  },
  // 订单详情
  {
    path: '/orderinfo',
    component: Orderinfo
  },
  // 售后补货
  {
    path: '/aftereplenish',
    component: Aftereplenish
  },
  // 我的地址
  {
    path: '/myaddress',
    component: Myaddress
  },
  // 修改地址
  {
    path: '/address',
    component: Addresschg
  },
  // 添加地址
  {
    path: '/addressadd',
    component: Addressadd
  },
  // 仓库选择
  {
    path: '/storehouse',
    component: Storehouse
  },
  // 下级收益
  {
    path: '/lowerprofit',
    component: Lowerprofit
  },
  // 我的下级
  {
    path: '/mylower',
    component: Mylower
  },
  // 商家资料
  {
    path: '/storeinfo',
    component: Storeinfo
  },
  // 绑定手机号
  {
    path: '/bindingnumber',
    component: Bindingnumber
  },
  // 提现明细
  {
    path: '/tixinfo',
    component: Tixinfo
  },
  // 收益明细
  {
    path: '/shouyinfo',
    component: Shyinfo
  },
  // 佣金明细
  {
    path: '/yongjinfo',
    component: Yongjinfo
  },


  //支出明细
   {
  	path:'/usezhichu',
  	component: Usezhichu
  },

  //销售额
  {
  	path:'/sellxiaoshou',
  	component: Sellxiaoshou
  },
  {
    path: '/',
    redirect: '/home' // 重定向
  },
  {
    path: '*',
    redirect: '/home' // 重定向
  },
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next)=>{
  // if (to.meta.auth){
      // if (window.localStorage.getItem("token")){
      //     next();
      // } else {
      //     next("/login");
      // }
  // } else {
      
  // }
  if(window.localStorage.getItem("state")==0 || window.localStorage.getItem("state")=="undefined" ){
    // next({ path: '/bindingnumber' })
    next()
  }else{
    next();
  }
})

export default router
