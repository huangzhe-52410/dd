import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cart_num:Boolean(localStorage['cart_num'])?localStorage['cart_num']:''
  },
  mutations: {
    setCartNum(state,num){
      console.log(num);
      state.cart_num=num
      localStorage.setItem('cart_num',num)
    }
  },
  actions: {
  },
  modules: {
  }
})
