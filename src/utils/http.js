import axios from 'axios'
axios.defaults.timeout = 5000; //请求超时5秒
axios.defaults.baseURL ="http://dongdong.shenjiyan.com";//"http://dongdong.shenjiyan.com"  //请求base url

export function post(url, data = {}){
    return new Promise((resolve, reject) => {
       axios.post(url, data)
       .then(response => {
         resolve(response.data);
       }, err => {
         reject(err)
       })
    })
  }