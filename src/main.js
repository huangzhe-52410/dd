import 'babel-polyfill';
import Es6Promise from 'es6-promise'
Es6Promise.polyfill()
// import vConsole from "vconsole";
// var vC = new vConsole();
// Vue.use(vC)
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from "axios";
import qs from "qs";

Vue.prototype.$qs = qs
import 'lib-flexible'// 自适应



axios.defaults.baseURL = 'http://dongdong.shenjiyan.com';


import { Uploader, Area, Toast, PullRefresh, List, Tab, Tabs, Popup, Field, DatetimePicker, Picker, Card, SubmitBar, Checkbox, CheckboxGroup,Stepper,SwipeCell,Button,Tag    } from 'vant'; // 引入vant部分组件
Vue.use(Uploader).use(Area).use(Toast).use(PullRefresh).use(List).use(Tab).use(Tabs).use(Popup).use(Field).use(DatetimePicker).use(Picker).use(Card).use(SubmitBar).use(Checkbox).use(CheckboxGroup).use(Stepper).use(SwipeCell).use(Button).use(Tag);

Vue.prototype.$IP = 'http://dongdong.shenjiyan.com'

//生产环境让console.log()为空函数
if (process.env.NODE_ENV === 'production') {
  console.log = () => { }
}

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
