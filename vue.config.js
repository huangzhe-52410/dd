

module.exports = {
  productionSourceMap:false,
  transpileDependencies: ["swiper","vant"], 
  devServer: {
    // overlay: {

    // },
    // 请求代理
    // proxy:{
    //   '/index': {
    //     target:'http://dongdong.shenjiyan.com',
    //     changeOrigin: true,
    //     pathRewrite: {
    //       '^/index': '/index' 
    //      }
    //   }
    // }
  },
  css: {
    loaderOptions: {
      css: {},
      postcss: {
        plugins: [
          require('postcss-px2rem')({
            remUnit: 37.5
          })
        ]
      }
    }
  },
  lintOnSave: false, // 关了eslint 检查
  // presets: [
  //   ['@vue/app', {
  //     polyfills: [
  //       'es.promise',
  //       'es.symbol'
  //     ]
  //   }]
  // ],
  transpileDependencies:['vant','swiper','es6-promise','axios','core-js']
}
